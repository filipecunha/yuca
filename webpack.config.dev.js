const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HappyPack = require('happypack');
const settings = require('./package.json').settings;

const src = path.join(__dirname, 'src');
require('babel-polyfill');

module.exports = {
  cache: true,
  entry: {
    index: path.join(src, 'index.pug'),
    app: [
      'babel-polyfill',
      'webpack-dev-server/client?http://localhost:3000',
      'webpack/hot/only-dev-server',
      './src/assets/scripts/main.js',
    ],
  },
  devtool: 'cheap-eval-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['happypack/loader?id=js'],
      },
      {
        test: /\.css$/,
        loaders: ['happypack/loader?id=css'],
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: ['happypack/loader?id=sass'],
      },
      {
        test: /\.(png|jpe?g|gif|webm|mp4|ogv|txt|mp3|ogg|wav|pdf)$/,
        loader: 'file-loader',
        options: {
          context: path.resolve(__dirname, './src'),
          name: '[path][name].[ext]',
        },
      },
      {
        test: /\.pug$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'html-loader',
            options: {
              attrs: [
                'img:src',
                'img:srcset',
                'source:src',
                'source:srcset',
              ],
            },
          }, {
            loader: 'pug-html-loader',
            options: {
              cache: true,
              data: {
                settings,
              },
            },
          },
        ],
      }, {
        test: /\.csv$/,
        exclude: /node_modules/,
        use: ['dsv-loader?delimiter=;'],
      },
      {
        test: /\.json$/,
        exclude: /node_modules/,
        use: ['json-loader'],
      },
      {
        test: /\.(eot|otf|woff|woff2|ttf|svg)$/,
        use: ['url-loader?name=fonts/[name].[ext]'],
      },
    ],
  },
  devServer: {
    port: 3000,
    hot: true,
  },
  externals: {
    jquery: 'jQuery',
  },
  plugins: [
    new HappyPack({
      id: 'css',
      loaders: [
        'style-loader',
        {
          loader: 'css-loader',
        },
      ],
    }),
    new HappyPack({
      id: 'sass',
      loaders: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            root: './dist/images',
          },
        },
        {
          loader: 'postcss-loader',
          options: {
            plugins() {
              return [autoprefixer({
                browsers: ['> 1%', 'last 2 versions'],
              })];
            },
          },
        },
        {
          loader: 'sass-loader',
        },
        {
          loader: 'import-glob-loader',
        },
      ],
    }),
    new HappyPack({
      id: 'js',
      loaders: [
        {
          loader: 'babel-loader',
          options: {
            env: {
              test: {
                plugins: ['istanbul'],
              },
            },
          },
          exclude: /node_modules/,
          include: path.resolve(process.cwd(), 'src'),
        },
      ],
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      ga: 'ga',
    }),
    new CopyWebpackPlugin([
      {
        from: './src/assets/images',
        to: './assets/images',
      },
    ]),
    new HtmlWebpackPlugin({
      title: settings.pageTitle,
      template: './src/index.pug',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
  output: {
    filename: 'bundle.[name].js',
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
  },
};
