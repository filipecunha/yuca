const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const settings = require('./package.json').settings;

module.exports = {
  entry: {
    app: './src/assets/scripts/main.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
          }],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
              },
            },
          ],
        }),
      },
      {
        test: /\.scss$/,
        enforce: 'pre',
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                discardComments: {
                  removeAll: true,
                },
                minimize: true,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins() {
                  return [autoprefixer({
                    browsers: ['> 1%', 'last 2 versions'],
                  })];
                },
              },
            },
            'sass-loader',
            {
              loader: 'import-glob-loader',
            },
          ],
        }),
      },
      {
        test: /\.(png|jpe?g|gif|webm|mp4|ogv|txt|mp3|ogg|wav|pdf)$/,
        loader: 'file-loader',
        options: {
          context: path.resolve(__dirname, './src'),
          name: settings.isNovoGCN ? '[name].[ext]' : '[path][name].[ext]',
        },
      }, {
        test: /\.csv$/,
        use: ['dsv-loader?delimiter=;'],
      },
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              attrs: [
                'img:src',
                'img:srcset',
                'source:src',
                'source:srcset',
              ],
              removeComments: false,
            },
          }, {
            loader: 'pug-html-loader',
            options: {
              exports: false,
              data: {
                settings,
              },
            },
          },
        ],
      },
      {
        test: /\.json$/,
        use: ['json-loader'],
      },
      {
        test: /\.(eot|otf|woff|woff2|ttf|svg)$/,
        use: ['url-loader?name=fonts/[name].[ext]'],
      },
    ],
  },
  externals: {
    jquery: 'jQuery',
    ga: 'ga',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: settings.pageTitle,
      template: path.join('./src', 'index.pug'),
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      ga: 'ga',
    }),
    new ExtractTextPlugin('main.css'),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        screw_ie8: true,
        warnings: false,
      },
    }),
  ],
  output: {
    filename: 'bundle.[name].js',
    path: path.resolve('dist'),
    publicPath: './',
  },
};
