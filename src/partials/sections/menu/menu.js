document.addEventListener("DOMContentLoaded", function(event) {
    createScrollListener();
    createButtonsListener();
});

createScrollListener = () => {
    const APARTMENT = 'APARTMENT';
    const SERVICES = 'SERVICES';
    const ADVANTAGES = 'ADVANTAGES';

    const apartmentAnchor = Math.round($('#apartments-section').offset().top - 80);
    const serviceAnchor = Math.round($('#services-section').offset().top - 80);
    const advantagesAnchor = Math.round($('#advantages-section').offset().top - 80);

    let actualSection = '';


    $(window).bind('scroll', () => {
        const scrollY = window.scrollY;

        if(scrollY >= advantagesAnchor) {
            actualSection = ADVANTAGES;
            activateButton('advantages-menubtn');
        }
        else if (scrollY >= serviceAnchor) {
            actualSection = SERVICES;
            activateButton('services-menubtn');
        }
        else if (scrollY >= apartmentAnchor) {
            actualSection = APARTMENT;
            activateButton('apartments-menubtn');
        }
        else if (scrollY <= apartmentAnchor) {
            actualSection = '';
            activateButton('');
        }
    })
};

createButtonsListener = () => {
    $('#apartments-menubtn').click(onClickButton);
    $('#services-menubtn').click(onClickButton);
    $('#advantages-menubtn').click(onClickButton);
};

scrollTo = (position) => {
    const body = $("html, body");

    body.stop().animate({ scrollTop: position }, 500, 'swing');
};

const activateButton = (buttonName) => {
    // console.log(buttonName);
    $('#apartments-menubtn').removeClass('active');
    $('#services-menubtn').removeClass('active');
    $('#advantages-menubtn').removeClass('active');

    $(`#${buttonName}`).addClass('active')
};

const onClickButton = (evt) => {
    const apartmentAnchor = $('#apartments-section').offset().top - 80;
    const serviceAnchor = $('#services-section').offset().top - 80;
    const advantagesAnchor = $('#advantages-section').offset().top - 79;

    const button = $(evt.currentTarget).attr('id');

    if(button === 'apartments-menubtn')
        scrollTo(apartmentAnchor);
    else if(button === 'services-menubtn')
        scrollTo(serviceAnchor);
    else if(button === 'advantages-menubtn')
        scrollTo(advantagesAnchor);
};
