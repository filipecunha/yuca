document.addEventListener("DOMContentLoaded", function(event) {
    startAutomaticFlip();

    if(!detectMobile()){
        flipOnHover();
    }
});

const detectMobile = () => {
    return navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
        || false;
};

const flipOnHover = () => {
    const flipBoxList = $('.flip-box');

    flipBoxList.map((index) => {
        const box = $(flipBoxList[index]);
        box.addClass('flip-box-desktop')
    });
};

const startAutomaticFlip = () => {
    const flipBoxList = $('.flip-box');
    const randomNumber = Math.round(Math.random() * (flipBoxList.length - 1)) + 1;

    const box = $(flipBoxList[randomNumber]);
    setTimeout(() => {
        box.addClass('flip-box-turn')
    }, Math.random() * 1000);

    setTimeout(() => {
        box.removeClass('flip-box-turn');
        startAutomaticFlip()
    }, (Math.random() * 3000) + 2000);
};
