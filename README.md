# Processo seletivo Yuca
---

###Como rodar o projeto

Após clonar o repositório ou baixar os arquivos, excute os seguintes comando no terminal, dentro da pasta do projeto:

`npm install` instala dependências

`npm start` inicia o projeto na porta 3000

`npm run build`  para gerar a pasta ./dist para produção

Para visualizar o projeto finalizado, entre em http://lipao.com.br/labs/yuca/

:)
